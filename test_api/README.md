# README

This **DOCUMENT** covers the **TESTAPI** used for the **Recrypt MULTI-FACTOR-AUTHENTICATION Spike**
This Api was meant to replicate a barebones environment with devise similar to how Recrypt operates.

This api uses the following specific gems:

* devise
* devise-jwt: version 0.6.0
* devise-two-factor: version 3.1.0
* rack-cors
* dotenv
* rqrcode

1) Devise is used for handling the user account services such as sign in, sign out and register.

2) Devise-jwt allows devise to be used as an api and accept requests from a frontend application.

3) Devise-two-factor is the gem that handles the two-factor-authentication in the backend when signing in.

4) Dotenv is used for applying environmental variables to the User Model.

5) Rqrcode is used for turning a otp_secret uri into a QR code image to be scanned.



**Important Links**
***************
https://github.com/heartcombo/devise

https://github.com/waiting-for-dev/devise-jwt

https://github.com/tinfoil/devise-two-factor

https://github.com/whomwah/rqrcode




**IMPORTANT CHANGES FOR DEVISE-TWO-FACTOR**
***************************************
It recommends a rqrcode-rails3 gem but that is outdated.

Some of the instructions as you go along in general are outdated.

READ the UPGRADING.md document as well as to why I added an additional change to the user model
with "attr_encrypted :otp_secret".
