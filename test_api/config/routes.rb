Rails.application.routes.draw do
  scope defaults: { format: 'json'} do
    devise_for :users

    authenticate :user do
      get 'two-factor-code', action: :show, controller: 'two_factor'
      put 'two-factor-active', action: :enable_otp, controller: 'two_factor'
      put 'two-factor-disable', action: :disable_otp, controller: 'two_factor'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
