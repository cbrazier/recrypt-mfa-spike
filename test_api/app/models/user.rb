class User < ApplicationRecord
  attr_encrypted :otp_secret,
    :key        =>  ENV['TWO_FACTOR_KEY'],
    :mode       => :per_attribute_iv_and_salt,
    :algorithm  => 'aes-256-cbc'

  devise :two_factor_authenticatable,
         :otp_secret_encryption_key => ENV['TWO_FACTOR_KEY']

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :registerable, :lockable,
         :confirmable, :recoverable, :rememberable, :validatable,
         :jwt_authenticatable, jwt_revocation_strategy: JwtBlacklist
end
