require 'rqrcode'

class TwoFactorController < ApplicationController

  def show
    generate_code
  end

  def enable_otp
    if(current_user.validate_and_consume_otp!(params[:code]))
      current_user.otp_required_for_login = true
      current_user.save!

      render json: {'2fa' => "success"}
    else 
      render json: {'2fa' => "failure"}
    end
  end

  def disable_otp
    current_user.otp_required_for_login = false
    current_user.save!
  end

  private 

  def generate_code 
    current_user.otp_secret ||= User.generate_otp_secret
    current_user.save!
    issuer = "test app"
    label = "#{issuer}:#{current_user.email}"
    uri = current_user.otp_provisioning_uri(label, issuer: issuer)
    qrcode = RQRCode::QRCode.new(uri)

    svg = qrcode.as_svg(
      offset: 0,
      color: '000',
      shape_rendering: 'crispEdges',
      module_size: 6,
      standalone: true
    );

    render json: {'qr' => svg}
  end
end