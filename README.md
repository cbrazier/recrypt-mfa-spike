**RECRYPT-MFA-SPIKE**

This project was created to demonstrate multi-factor authorization on a rails api similar to what Recrypt currently has.

It uses a basic React Frontend application that uses api calls to allow the user to sign in, sign out and activate/disable MFA.

The rails API relies mainly on devise and two of its extensions to handle the api requests and the MFA.


**HOW TO USE**

1) Once your environment is setup and you have your rails server running, your postgres database setup and your react server running you may proceed.

2) Create a user in rails console: User.create(email: "dummyemail@email.com", password: "password);

3) Confirm the user in rails console: User.first.confirm.

4) Sign in on the frontend side in browser only filling out the username and password fields. (The otp_attempt is ignored at this point).

5) Once logged in Click on the MFA link at the top.

6) This page will present to you a QR code image and a input box. Scan the Image into your Google Authenticator App to recieve a code.

7) Input the code into the bar and submit if it fails a error will be displayed below because it is out of date or wrong.

8) If successfull no error will show and you have activated MFA on your account. You can then logout through the link.

9) Try to login in with random strings or numbers. Nothing should happen and in the browsers network terminal 401's should be recieved.

10) Input the correct code from your Google Authenticator App to proceed as well as your email and password.