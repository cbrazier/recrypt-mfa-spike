import React from 'react';
import Storage from "./Storage";

export const Login = ({ setLoggedIn }) => {

  const emailRef = React.createRef();
  const passwordRef = React.createRef();
  const otpRef = React.createRef();

  const url = "http://localhost:3000/users/sign_in.json";

  const getDetails = (event) => {
    event.preventDefault();
    const user = {
      user: {
        email: (emailRef.current.value),
        password: (passwordRef.current.value),
        otp_attempt: (otpRef.current.value)
      }
    }

    const result = new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("POST", url);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify(user));

      xhr.onload = () => {
        const json = JSON.parse(xhr.responseText || 'null');
        if(xhr.status < 200 || xhr.status >= 400){ reject(json)}
        const authorization = xhr.getResponseHeader('Authorization');
        resolve({ 'authorization': authorization });
      }
    })

    result.then(response => {
     Storage.setStorage("jwt", response.authorization);
     setLoggedIn(true);
    })

  }

  return (
    <div align="center" className="login-container">
      <div className="title">
        <h1>Login</h1>
      </div>
      <div className="body">
        <form onSubmit={getDetails}>
          <p>Email</p>
          <input name="Email" ref={emailRef} type="email" placeholder="Email" />
          <p>Password</p>
          <input name="password" ref={passwordRef} type="password" placeholder="password" />
          <p>Otp Code</p>
          <input name="Otp code" ref={otpRef} type="text" placeholder="Otp Code" />
          <br />
          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  )
}
