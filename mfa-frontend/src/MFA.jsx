import React, { useState, useEffect } from 'react';
import Storage from "./Storage";;

export const MFA = () => {
  
  const [qRImage, setQRImage] = useState(null);
  const [status, setStatus] = useState(null);
  const otpRef = React.createRef();
  const jwt = Storage.getStorage("jwt");
  const codeUri = "http://localhost:3000/two-factor-code.json";
  const tfURI = "http://localhost:3000/two-factor-active.json";
  const disableURI = "http://localhost:3000/two-factor-disable.json";

  useEffect(() => {
     fetch(codeUri, {
      method: "GET",
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Authorization: jwt
      },
    }).then(async(response) => {
      if(response.status === 200){
        const result = await response.json();
        setQRImage(result.qr);
      }
    });
  }, [codeUri])

  useEffect(() => {
    if(qRImage !== null){
      const parse = new DOMParser();
      const doc = parse.parseFromString(qRImage, "image/svg+xml");
      const svgElement = document.getElementById("svg");
      svgElement.innerHTML = doc.documentElement.innerHTML;
      setQRImage(null)
    }
  }, [qRImage])

  const sendOTP = (event) => {
    event.preventDefault();
    const code = {
      code: otpRef.current.value
    };

    fetch(tfURI, {
      method: "PUT",
      mode: 'cors',
      body: JSON.stringify(code),
      headers: {
        'Content-Type': 'application/json',
        Authorization: jwt
      },
    }).then(async(response) => {
      if(response.status === 200){
          const result = await response.json();
          if(result["2fa"] !== "success"){
            setStatus("Incorrect Code Try Again!");
          } else {
            setStatus(null);
          }
      }
    })
  }

  const disableOTP = (event) => {
    event.preventDefault();

    fetch(disableURI, {
      method: "PUT",
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Authorization: jwt
      },
    }).then(async(response) => {
      if(response.status === 200){

      }
    })
  }

  return (
    <div align="center" className="otp-container">
    <div className="title">
      <h1>MFA</h1>
    </div>
    <div className="body">
      <div>
        <svg xmlns="http://www.w3.org/2000/svg" height="325px" width="325px" id="svg" />
      </div>
      <div className="qrCode-input">
        <span>Input Code</span>
        <input type="text" ref={otpRef} />
        <button onClick={sendOTP}>Submit</button>
      </div>
      <div>
        <button onClick={disableOTP}>Disable OTP</button>
      </div>
      <br />
      <div>
        {status !== null && <p>{status}</p>}
      </div>
    </div>
  </div>
  )
}
