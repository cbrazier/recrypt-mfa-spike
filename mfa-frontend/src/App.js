import React, { useState } from 'react';
import './App.css';
import { Login } from './Login';
import { MFA } from './MFA';
import { Logout } from './Logout';

const App = () => {

  const [loggedIn, setLoggedIn] = useState(false);
  const [page, setPage] = useState("Login")

  const renderPage = () => {
    if((page === "Login" && !loggedIn) || 
      (page === "Logout" && !loggedIn)) return <Login setLoggedIn={setLoggedIn}/>;
    if(page === "MFA" && loggedIn) return <MFA />;
    if((page === "Logout" && loggedIn) || 
      (page === "Login" && loggedIn)) return <Logout setLoggedIn={setLoggedIn} />;
    return null
  }

  return (
    <div>
      <div className="nav">
        {loggedIn && <span onClick={() => setPage("MFA")}>MFA</span>}
        {!loggedIn ? 
          <span onClick={() => setPage("Login")}>Login</span> 
          : 
          <span onClick={() => setPage("Logout")}>Logout</span>}
      </div>
      <div className="page">
        {renderPage()}
      </div>
    </div>
  )
  // const [qRImage, setQRImage] = useState(null);
  // const emailRef = React.createRef();
  // const passwordRef = React.createRef();
  // const url = "http://localhost:3000/users/sign_in.json";

  // useEffect(() => {
  //   if(qRImage !== null){
  //     const parse = new DOMParser();
  //     const doc = parse.parseFromString(qRImage, "image/svg+xml");
  //     const svgElement = document.getElementById("svg");
  //     svgElement.innerHTML = doc.documentElement.innerHTML;
  //     setQRImage(null)
  //   }
  // }, [qRImage])

  // const getDetails = (event) => {
  //   event.preventDefault();
  //   const user = {
  //     user: {
  //       email: (emailRef.current.value),
  //       password: (passwordRef.current.value)
  //     }
  //   }

  //   fetch(url, {
  //     method: "POST",
  //     mode: 'cors',
  //     body: JSON.stringify(user),
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     credentials: 'omit'
  //   }).then(async(response) => {
  //     if(response.status === 200){
  //       setLoggedIn(true);
  //       const result = await response.json();
  //       setQRImage(result.qr);
  //     }
  //   });
  // }


  // const logout = () => {
  //   const url = "http://localhost:3000/users/sign_out.json";
  //   fetch(url, {
  //     method: "DELETE",
  //     mode: 'cors',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: ''
  //     }
  //   }).then(response => {
  //     if(response.status === 200 || 204 ){
  //       setLoggedIn(false);
  //     }
  //   });
  // }

  // if(!loggedIn){
  //   return (
  //     <div align="center" className="login-container">
  //       <div className="title">
  //         <h2>Login</h2>
  //       </div>
  //       <div className="body">
  //         <form onSubmit={getDetails}>
  //           <p>Email</p>
  //           <input name="Email" ref={emailRef} type="email" placeholder="Email" />
  //           <p>Password</p>
  //           <input name="password" ref={passwordRef} type="password" placeholder="password" />
  //           <button type="submit">Submit</button>
  //         </form>
  //       </div>
  //     </div>
  //   );
  // } else {
  //   return(
  //     <div align="center" className="logout-container">
  //       <div className="title">
  //         <h2>You are Currently Logged In</h2>
  //       </div>
  //       <div>
  //         <svg xmlns="http://www.w3.org/2000/svg" height="325px" width="325px" id="svg" />
  //       </div>
  //       <div className="qrCode-input">
  //         <span>Input Code</span>
  //         <input type="text" />
  //         <button>Submit</button>
  //       </div>
  //       <button onClick={() => logout()}>Logout</button>
  //     </div>
  //   );
  // }
}

export default App;
