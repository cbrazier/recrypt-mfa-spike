import React from 'react';
import Storage from "./Storage";

export const Logout = ({ setLoggedIn }) => {


  const logout = () => {
    const url = "http://localhost:3000/users/sign_out.json";
    fetch(url, {
      method: "DELETE",
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Authorization: ''
      }
    }).then(response => {
      if(response.status === 200 || 204 ){
        Storage.clearStorage();
        setLoggedIn(false);
      }
    });
  }

  return (
    <div align="center" className="logout-container">
      <div className="title">
        <h1>Logout</h1>
        <h2>You are Currently Logged In</h2>
      </div>
      <button onClick={() => logout()}>Logout</button>
    </div>
  )
}
