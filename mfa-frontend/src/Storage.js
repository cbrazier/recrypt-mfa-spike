const setStorage = (name, item) => window.localStorage.setItem(name, JSON.stringify(item));

const getStorage = (name) => {
  const item = window.localStorage.getItem(name);
  if(item === null){
    return false;
  }
  return JSON.parse(item);
}

const clearStorage = () => window.localStorage.clear();

export default { setStorage, getStorage, clearStorage };
